<?php
ini_set('memory_limit', '1024M');
str_pad('', 4096, "\n");

require __DIR__ . '/vendor/autoload.php';
use phpish\shopify;

require __DIR__ . '/conf.php';


$allps = file_get_contents('allpass.json');
$allpass = json_decode($allps, true);

class SimpleXMLExtended extends SimpleXMLElement
{
    public function addCData($cdata_text)
    {
        $node = dom_import_simplexml($this);
        $no = $node->ownerDocument;
        $node->appendChild($no->createCDATASection($cdata_text));
    }
}

foreach ($allpass as $item)
{

  if ($item['shop']  == 'sappatos.myshopify.com' || $item['shop'] == 'macon-clothing.myshopify.com'  || $item['shop'] == 'whiteroom-korinthos.myshopify.com') {

    } else {
      $shopify = shopify\client($item['shop'], SHOPIFY_APP_API_KEY, $item['authtoken']);
      try
      {
        //Take all products with loop
        $productsex = [];
        $count = $shopify("GET /admin/products/count.json");
        if ($count > 0)
        {
          $pages = ceil($count / 100);

          for ($i = 0;$i < $pages;$i++)
          {
            # Making an API request can throw an exception
            $productlimit = $shopify('GET /admin/products.json?limit=100&published_status=published&page='.($i + 1).'&fields=id,title,vendor,product_type,handle,variants,image');

            foreach ($productlimit as $result)
            {
              array_push($productsex, $result);
            }
          }
        }


        //decode the data
        $jsonFile_decoded = json_encode($productsex);

        $jsonFile_decoded = json_decode($jsonFile_decoded);


        //create a new xml object
        // Take the contents back to the file
        // $inp = file_get_contents('allpass.json');
        // $tempArray = json_decode($inp, true);

        //Check if this shop installed the app
        //take the availability  of json
        $availability = $item['availability'];

        $xml = new SimpleXMLExtended('<mywebstore/>');
        //loop through the data, and add each record to the xml object
        $datenow = date("Y-m-d H:i:s");
        $dataproducts = $xml->addChild('products');
        $productadd = $dataproducts->addChild('created_at', $datenow);

        foreach ($jsonFile_decoded AS $products)
        {

          foreach($products AS $key => $product)
          {
            if ($key == 'id')
            { 
              $id = $product;
            }
            if ($key == 'title')
            {
              $title = $product;
            }
            if ($key == 'handle')
            {
              $handle = $product;
            }
            if ($key == 'image')
            {
              if ($product)
              {
                foreach($product AS $key => $image)
                {
                  if ($key == 'src')
                  {
                    $imageUrl = $image;
                  }
                }
              }
            }
            if ($key == 'product_type')
            {
              $product_type = $product;
            }
            if ($key == 'vendor')
            {
              if (strlen($product) > 0)
              {
                $manufacturer = $product;
              }
              else
              {
                $manufacturer = 'Please add vendor on this product';
              }
            }
            if ($key == 'variants')
            {
              foreach($product[0] AS $key => $skus)
              {
                if ($key == 'price')
                {
                  $price = $skus;
                }
                if ($key == 'sku')
                {
                  $skuVal = $skus;
                }
                if ($key == 'weight')
                {
                  $weight = $skus;
                }
                if ($key == 'inventory_quantity')
                {
                  $inventory = intval($skus);
                  if ($inventory > 0)
                  {
                    $instocks = Y;
                  }
                  else
                  {
                    $instocks = N;
                  }
                }
                
                if ($key == 'inventory_management')
				{
                    if ($skus == null)
					{
					    $inventorymanagement = 'Y';
					}
					 else
					{
					    $inventorymanagement = 'N';
					}
				}
                
                if ($key == 'barcode')
                {
                  $eannumber = preg_replace("/[^0-9]/", "", $skus);
                  $lenghtnumber = strlen($eannumber);
                }
              }
            }


            if ($key == 'options'){
              foreach($product AS $key => $options)
              {
                $existsize = 'false';
                $existcolor = 'false';

                foreach($options AS $key => $item) {
                  if ($key == 'name') {
                    if ($item == 'ΜΕΓΕΘΟΣ') {
                      $existsize = 'true';
                    }
                  }
                  if ($key == 'values' and $existsize == 'true') {
                    $size = rtrim(implode(',', $item), ',');
                  }
                  if ($key == 'name') {
                    if ($item == 'ΧΡΩΜΑ') {
                      $existcolor = 'true';
                    }
                  }
                  if ($key == 'values' and $existcolor == 'true') {
                    $color = rtrim(implode(',', $item), ',');
                  }
                }


              }
            }

            if($key == 'images'){
              if (isset($product[1])  ) {
                foreach($product[1] AS $key => $imgs){
                  if ($key == 'src')
                  {
                    $images = $imgs;
                  } else {
                    $images = ' ';
                  }
                }
              }
            }


          }
          if ($inventory > 0 ||  $inventorymanagement == 'Y')
          {
            $productadd = $dataproducts->addChild('product');

            $productadd->id = NULL;
            $productadd->id->addCData($id);
            $productadd->name = NULL;
            $productadd->name->addCData($title);
            $uriproduct = 'https://' . $item['shop'] . '/products/' . $handle;
            $productadd->link = NULL;
            $productadd->link->addCData($uriproduct);
            $productadd->image = NULL;
            $productadd->image->addCData($imageUrl);
            $productadd->additional_imageurl = NULL;
            $productadd->additional_imageurl->addCData($images);
            $productadd->category = NULL;
            $productadd->category->addCData($product_type);
            $productadd->price_with_vat = NULL;
            $productadd->price_with_vat->addCData($price);
            $productadd->manufacturer = NULL;
            $productadd->manufacturer->addCData($manufacturer);
            $productadd->mpn = NULL;
            $productadd->mpn->addCData($skuVal);
            $productadd->weight = NULL;
            $productadd->weight->addCData($weight);
            $productadd->instock = NULL;
            $productadd->instock->addCData($instocks);
            if ($lenghtnumber > 0)
            {
              $productadd->ean = NULL;
              $productadd->ean->addCData($eannumber);
            }
            else
            {
              $productadd->ean = NULL;
              $productadd->ean->addCData('  ');
            }
            $productadd->availability = NULL;
            $productadd->availability->addCData($availability);
            $productadd->size = NULL;
            $productadd->size->addCData($size);
            $productadd->color = NULL;
            $productadd->color->addCData($color);
          }
        }

        //output the xml file
        //create xml with proucts
        $nameShop = $item['shop'];
        $nameShop = str_replace(".myshopify.com", "", $nameShop);
        $xml->asXML("xmlallsite/products-" . $nameShop . ".xml");

      }
      catch(shopify\ApiException $e)
      {
        # HTTP status code was >= 400 or response contained the key 'errors'
        echo $e;
        print_r($e->getRequest());
        print_r($e->getResponse());

        //If unistall the app delete file xml and  authtoken
        if (strpos($e, 'Invalid API key or access token') !== false) {
          $allpasClear = [];
          foreach ($allpass as $element)
          {
            //check the property of every element
            if (!($item['shop'] == $element['shop']))
            {
              array_push($allpasClear, $element);
            }
          }
          $jsonData = json_encode($allpasClear);
          file_put_contents('allpass.json', $jsonData);

          $nameShop = $item['shop'];
          $nameShop = str_replace(".myshopify.com", "", $nameShop);
          rename("xmlallsite/products-" . $nameShop . ".xml", "xmlallsite/uninstallXml/products-" . $nameShop . ".xml");
        }




      }
      catch(shopify\CurlException $e)
      {
        # cURL error
        echo $e;
        print_r($e->getRequest());
        print_r($e->getResponse());
      }
    }
}
