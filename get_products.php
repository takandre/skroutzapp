<?php

ini_set('memory_limit', '1024M');
//Fixing Cloudflare Error 524 send 4kb of new line to browser, just make sure that this new line will not affect your code.
str_pad('', 4096, "\n");




require __DIR__ . '/vendor/autoload.php';
use phpish\shopify;
require __DIR__ . '/conf.php';


//Take the origin of shop and credensial of customer if made AJAX post of Javascripts

if ( !empty( $_POST['shop']) ){
   $nameShop = $_POST['shop'];
   $titleShop = $_POST['titleShop'];
   $currentauthtoken = $_POST['currentauthtoken'];
   $currentavailability = $_POST['currentavailability'];


    $shopify = shopify\client($nameShop, SHOPIFY_APP_API_KEY, $currentauthtoken);
    //Connect with Shopify Api
    $count = $shopify("GET /admin/products/count.json");
		$productsex = [];
		if ($count > 0)
		{
			$pages = ceil($count / 100);
			for ($i = 0; $i < $pages; $i++)
			{
				// Making an API request can throw an exception
				$productlimit = $shopify('GET /admin/products.json?limit=100&published_status=published&page='.($i + 1).'&fields=id,title,vendor,product_type,handle,tags,variants,image,options,images');
				foreach($productlimit as $result)
				{
  				array_push($productsex, $result);
				}
			}
		}

		// decode the data
		$jsonFile_decoded = json_encode($productsex);
		$jsonFile_decoded = json_decode($jsonFile_decoded);

		// create a new xml object
		class SimpleXMLExtended extends SimpleXMLElement
       {
			public function addCData($cdata_text)
			{
				$node = dom_import_simplexml($this);
				$no = $node->ownerDocument;
				$node->appendChild($no->createCDATASection($cdata_text));
			}
		}


		$xml = new SimpleXMLExtended('<mywebstore/>');
		// loop through the data, and add each record to the xml object
		$datenow = date("Y-m-d H:i:s");
		$dataproducts = $xml->addChild('products');
		$productadd = $dataproducts->addChild('created_at', $datenow);
		foreach($jsonFile_decoded AS $products)
		{
            $instocks = 'N';
			$inventorymanagement = 'N';
			$continueSale = 'N';
			foreach($products AS $key => $product)
			{
			   
				if ($key == 'id')
				{
					$id = $product;
				}
				if ($key == 'title')
				{
					$title = $product;
				}
				if ($key == 'handle')
				{
					$handle = $product;
				}

				if ($key == 'image')
				{
					if ($product)
					{
						foreach($product AS $key => $image)
						{
							if ($key == 'src')
							{
								$imageUrl = $image;
							}
						}
					}
				}
				if ($key == 'product_type')
				{
					$product_type = $product;
				}

				if ($key == 'vendor')
				{
                  if (strlen($product) > 0)
                  {
                    $manufacturer = $product;
                  }
                  else
                  {
                   $manufacturer = 'Please add vendor on this product';
                  }
		        }

                if($key == 'tags'){
                   if (strpos($product, "χρωμα:") !== false) {
                    $colors = explode("χρωμα:", $product );
                    $color_final = explode("=", $colors[1] );
                    $color = $color_final[0];
                  }
                }
               
				if ($key == 'variants')
				{
					foreach($product AS $key => $variants)
					{
					  
					  foreach($variants AS $key => $skus)
					  {
					     
						if ($key == 'price')
						{
							$price = $skus;
						}
						if ($key == 'sku')
						{
							$skuVal = $skus;
						}
						if ($key == 'weight')
						{
							$weight = $skus;
						}
						if ($key == 'inventory_quantity')
						{
						    
							$inventory = intval($skus);
							if ($inventory > 0)
							{
								$instocks = 'Y';
							}
						}
						
						if ($key == 'inventory_management')
						{
							if ($skus ==  null)
							{
								$continueSale = 'Y';
							}
						}
						
						
					
					    if ($key == 'inventory_policy')
						{
						   print_r($skus);
							if ($skus == 'continue')
							{
								$inventorymanagement = 'Y';
							}
						}
						
						if ($key == 'barcode')
						{
							$eannumber = preg_replace("/[^0-9]/", "", $skus);
							$lenghtnumber = strlen($eannumber);
						}
					  }	
					}
				}

        if ($key == 'options'){
          foreach($product AS $key => $options)
          {
             $existsize = 'false';
             $existcolor = 'false';

             foreach($options AS $key => $item) {
               if ($key == 'name'){
                 if ($item == 'ΜΕΓΕΘΟΣ' || $item == 'Size'){
                    $existsize = 'true';
                 }
               }
               if ($key == 'values' and $existsize == 'true') {
                 $size = rtrim(implode(',', $item), ',');
               }

               if ($key == 'name') {
                 if ($item == 'ΧΡΩΜΑ') {
                    $existcolor = 'true';
                 }
               }
               if ($key == 'values' and $existcolor == 'true') {
                  $color = rtrim(implode(',', $item), ',');
               }
             }
          }
        }

        if($key == 'images'){
          if (isset($product[1])  ) {
            foreach($product[1] AS $key => $imgs){
              if ($key == 'src')
              {
                $images = $imgs;
              }
            }
          } else {
            $images = ' ';
          }
        }



	  }

      if ($instocks == 'Y' ||  $inventorymanagement == 'Y' || $continueSale ==  'Y')
      {
            $productadd = $dataproducts->addChild('product');
          	$productadd->id = NULL;
          	$productadd->id->addCData($id);
          	$productadd->name = NULL;
          	$productadd->name->addCData($title);
          	$uriproduct = 'https://' . $nameShop . '/products/' . $handle;
          	$productadd->link = NULL;
          	$productadd->link->addCData($uriproduct);
          	$productadd->image = NULL;
          	$productadd->image->addCData($imageUrl);
            $productadd->additional_imageurl = NULL;
            $productadd->additional_imageurl->addCData($images);
          	$productadd->category = NULL;
          	$productadd->category->addCData($product_type);
          	$productadd->price_with_vat = NULL;
          	$productadd->price_with_vat->addCData($price);
            $productadd->manufacturer = NULL;
            $productadd->manufacturer->addCData($manufacturer);
          	$productadd->mpn = NULL;
          	$productadd->mpn->addCData($skuVal);
          	$productadd->weight = NULL;
          	$productadd->weight->addCData($weight);
          	$productadd->instock = NULL;
          	$productadd->instock->addCData($instocks);
          	if ($lenghtnumber > 0)
          	{
          		$productadd->ean = NULL;
          		$productadd->ean->addCData($eannumber);
          	}
          	else
          	{
          		$productadd->ean = NULL;
          		$productadd->ean->addCData('  ');
          	}
          	$productadd->availability = NULL;
          	$productadd->availability->addCData($currentavailability);
            $productadd->size = NULL;
            $productadd->size->addCData($size);
            $productadd->color = NULL;
            $productadd->color->addCData($color);
      }
  }
  // create xml with proucts
  // output the xml file
  $xml->asXML("xmlallsite/products-" . $titleShop . ".xml");
}
