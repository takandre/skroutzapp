<?php
    $times = time(); 
    $today = date("F j, Y, g:i a");
    $none = 'none';
    //Cheack if customer has create Xml
    $fileXml = 'xmlallsite/products-'.$titleShop.'.xml';
    $onloadCreateXml = 'false';
    if( file_exists($fileXml) ){
      $Xml_exists = $xmlURL;
    } else {
      $Xml_exists = 'Xml Creation in progress';
      $onloadCreateXml = 'true';
    }
	               echo '<!DOCTYPE html>
                          <html lang="en" dir="ltr">
                            <head>

                              <meta charset="utf-8">
                              <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                              <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                              <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
                              <meta name="google-site-verification" content="phAvGqvt5JA99WZrR_OdCGo-PG5T4T9fYWk26RRRgD8" />

                              <meta name="description" content="App Skroutz xml for Shopify">
                              <title>App Skroutz xml for Shopify</title>
                              <!-- Css  -->
                              <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
                              <link rel="stylesheet" type="text/css" href="main.css?ts='.$times.'">
                              <!-- Css  -->
                              <!-- Javascripts -->
                              <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js">
                              </script>
                              <script>
                                //for copy in url of xml
                                $(function() {
                                  $("input.copy-text").click(function() {
                                    $(this).focus();
                                    $(this).select();
                                    document.execCommand("copy");
                                    $(".copied").text("Copied to clipboard").show().fadeOut(1200);
                                  });
                                  // Set form variable
                                  $(".form-availability").submit(function(event){
                                    event.preventDefault();
                                    var availabilityVal = $("#availabilityid").val();
                                    // Check if availabilityVal value set
                                    if ( $.trim(availabilityVal) != "" ){
                                      $("#button-addon4 img").removeClass("d-none");
                                      $("#button-addon4 button").prop("disabled", true);
                                      // Process AJAX request
                                      $.post("custom_availability.php",{name: availabilityVal,shopV:"'.$nameShop.'"}, function(data){
                                      }).done(function(data){
                                            $("#button-addon4 img").addClass("d-none");
                                            var saveButton= $("#button-addon4 button").html();
                                            $("#button-addon4 button").text("Saved Data in System Storage.");
                                            setTimeout(function(){
                                                $("#button-addon4 button").prop("disabled", false);
                                                $("#button-addon4 button").html(saveButton);
                                            }, 3000);
                                      });
                                    }
                                  });
                                  function postGetProducts() {
                                    $(".loadinggif").removeClass("d-none");
                                    $("#reloadpage").prop("disabled", true);
                                    var availabilityVal = $("#availabilityid").val();
                                    if ( $.trim(availabilityVal) == "" ){
                                      availabilityVal = "'.$currentavailability.'";
                                    }
                                    $.ajax({
                                      type: "POST",
                                      url: "get_products.php",
                                      data: {
                                        datas: true,
                                        shop:"'.$nameShop.'",
                                        titleShop:"'.$titleShop.'",
                                        currentauthtoken: "'.$currentauthtoken.'",
                                        currentavailability: availabilityVal
                                      },
                                      success: function(data) {
                                      console.log(data);
                                        $(".xmlurllink").attr("value", "'.$xmlURL.'");
                                        $(".loadinggif").addClass("d-none");
                                        var textButton= $("#reloadpage").html();
                                        $("#reloadpage").text("XML File Has Been Updated!");
                                        setTimeout(function(){
                                            $("#reloadpage").prop("disabled", false);
                                            $("#reloadpage").html(textButton);
                                        }, 3000);
                                      }
                                    });
                                  }
                                  $("#reloadpage").click(function() {
                                    postGetProducts();
                                  });
                                  if ("'.$onloadCreateXml.'" == "true") {
                                    postGetProducts();
                                  }

                                });
                              </script>

                              <script>
                              !function(f,b,e,v,n,t,s)
                              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";
                              n.queue=[];t=b.createElement(e);t.async=!0;
                              t.src=v;s=b.getElementsByTagName(e)[0];
                              s.parentNode.insertBefore(t,s)}(window, document,"script",
                              "https://connect.facebook.net/en_US/fbevents.js");
                              fbq("init", "207533189826901");
                              fbq("track", "PageView");
                             </script>
                             <noscript><img height="1" width="1" style="display:none"
                              src="https://www.facebook.com/tr?id=207533189826901&ev=PageView&noscript=1https://www.facebook.com/tr?id=207533189826901&ev=PageView&noscript=1"
                             /></noscript>

                              <!-- Javascripts -->
                            </head>
                            <body>
                              <div class="container">
                                <div class="row">
                                  <div class="col text-justify">
                                    <h1 class="font-weight-normal pt-5 mt-3 mb-3">Welcome to the Team !
                                    </h1>
                                    <h5 class="font-weight-light pb-2 mb-4">
                                    So excited to have you on board and looking forward to your insights.
                                    We already completed the process for you and your products were succesfully imported! Your XML feed is ready to copy below and you can change the default delivery time.
                                    You can go ahead and sync it now with your <a href="https://merchants.skroutz.gr/merchants/products/scans" target="_blank">Skroutz Merchant Account</a>. Skroutz will import and approve your
                                    products automatically. In case you have an approval issue please contact your Skroutz Merchant Account contact person.
                                    </h5>
                                    <hr>
                                  </div>
                                </div>
                              </div>
                              <section class="section-container">
                                <div class="container">
                                  <div class="row">
                                    <div class="col-md-5  justify-content-center align-self-center">
                                      <p class="small-p">
                                      <span class="text-dark h5">
                                        Your XML feed is ready to copy here:
                                        </span>
                                      </p>
                                    </div>
                                    <div class="col-md-7">
                                      <div class="bg-white  border pt-5 pb-4 pl-4 pr-4">
                                        <div class="container-copy">
                                          <div class="copied"></div>
                                          <img src="images/ajax-load.gif" alt="loader-gif" class="loadinggif d-none">
                                          <input type="text" class="form-control copy-text bg-primary text-light xmlurllink" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" value="'.$Xml_exists.'">
                                          <p class="medium-p text-center pt-1 m-0">Just click to copy automatically</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                              <section class="section-container">
                                <div class="container">
                                  <div class="row">
                                    <div class="col-md-5 justify-content-center align-self-center">
                                      <p>
                                        <span class="text-dark h5"> Just one thing... the default delivery time is:</span>
                                        <br>
                                        Available. Delivery time 1-3 days.
                                      </p>
                                    </div>
                                    <div class="col-md-7">
                                      <div class="bg-white  border pt-5 pb-4 pl-4 pr-4">
                                        <form method="post" action="custom_availability.php" class="form-availability text-center">
                                          <div class="input-group">
                                            <input type="text"  class="form-control" id="availabilityid" name="availability" placeholder="'.$currentavailability.'">
                                            <div class="input-group-append" id="button-addon4">
                                              <button type="submit" class="btn btn-success btn-sm">Save
                                              <img src="images/ajax-load.gif" alt="loader-gif" class="d-none">
                                              </button>
                                            </div>
                                          </div>
                                        </form>
                                        <p class="text-center pt-2 pb-1 m-0">
                                          Just click to change the default option
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                              <section class="section-container">
                                <div class="container">
                                  <div class="row">
                                    <div class="col-md-5 justify-content-center align-self-center">
                                      <p class="small-p">
                                        <span class="text-dark h5"> Details:</span>
                                        <br>
                                        Review the status and details of your XML Feed
                                      </p>
                                    </div>
                                    <div class="col-md-7">
                                      <div class="bg-white border p-4">
                                        <div class="row">
                                          <div class="col-md-6 justify-content-center align-self-center">
                                            Last update:
                                          </div>
                                          <div class="col-md-6 mt-3 mb-3 text-md-right">
                                            '.$today.'
                                          </div>
                                          <div class="col-md-6 col-md-5 justify-content-center align-self-center">
                                            XML Feed Update status:
                                          </div>
                                          <div class="col-md-6 text-md-right">
                                            <p class="btn btn-success btn-md mt-3 mb-3 mw-b">
                                              Success
                                            </p>
                                          </div>
                                          <div class="col-md-6 col-md-5 justify-content-center align-self-center">
                                            Real time monitoring:
                                          </div>
                                          <div class="col-md-6 text-md-right">
                                            <p class="btn btn-success btn-md mt-3 mb-3 mw-b">
                                              Success
                                            </p>
                                          </div>
                                          <div class="col-md-6 col-md-5 justify-content-center align-self-center">
                                            Counter of all products:
                                          </div>
                                          <div class="col-md-6 text-md-right">
                                            <p class="btn btn-success btn-md mt-3 mb-3 mw-b">
                                              '.$count.'
                                            </p>
                                          </div>
                                          <div class="col-md-6 col-md-5 justify-content-center align-self-center">
                                            Upate XML:
                                          </div>
                                          <div class="col-md-6 text-md-right">
                                            <button class="btn btn-outline-primary mt-3 mb-3" id="reloadpage">
                                              Update Now
                                              <img src="images/ajax-load.gif" alt="loader-gif" class="loadinggif d-none">
                                            </button>
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </section>
                              <section class="section-container">
                                <div class="container">
                                  <div class="row ">
                                    <div class="col">
                                      <div class="bg-white border text-center p-2">
                                        <p class="m-0">Need help?
                                          <a href="https://think-plus.gr/contact-us/" target="_blank">Click here
                                          </a>
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                              <!--
                              <section class="section-container">
                                <div class="container">
                                  <div class="row">
                                    <div class="col-md-12 text-center">
                                      <hr class="mt-3">
                                      <p class="m-0">Additional Feed?
                                      </p>
                                      <hr class="mt-3 mb-4">
                                    </div>
                                    <div class="col-12 text-center all-apps">
                                      <a href="/" target="_blank">
                                        <img src="images/icon_fb.png" alt="fb-icon">
                                      </a>
                                      <a href="/" target="_blank">
                                        <img src="images/icon_bestPrice.png" alt="bestPrice-icon">
                                      </a>
                                      <a href="/" target="_blank">
                                        <img src="images/icon_public.png" alt="public-icon">
                                      </a>
                                      <a href="/" target="_blank">
                                        <img src="images/icon_linkwise.png" alt="linkwise-icon">
                                      </a>
                                      <a href="/" target="_blank">
                                        <img src="images/icon_entersoft.png" alt="entersoft-icon">
                                      </a>
                                      <a href="/" target="_blank">
                                        <img src="images/icon_softone.png" alt="softone-icon">
                                      </a>
                                      <a href="/" target="_blank">
                                        <img src="images/icon_microsoft.png" alt="microsoft-icon">
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </section>
                              -->
                              <section class="section-container">
                                <div class="container">
                                  <div class="row ">
                                    <div class="col-md-12">
                                      <div class="bg-white  border text-center p-2">
                                        <p class="m-0">Custom development? Have a project in mind?
                                          <a href="https://think-plus.gr/contact-us/" target="_blank">Click here
                                          </a>
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                              <section class="section-container">
                                <div class="container">
                                  <div class="row text-center">
                                    <div class="col-md-12">
                                      <img class="icon-programmer pb-5" src="images/programmer.png" alt="programmer-icon p-2">
                                    </div>

                                    <div class="col-sm-12 text-center">
                                      <a href="https://think-plus.gr/%CF%80%CF%81%CE%BF%CF%83%CF%89%CF%80%CE%B9%CE%BA%CE%AC-%CE%B4%CE%B5%CE%B4%CE%BF%CE%BC%CE%AD%CE%BD%CE%B1/" target="_blank" class="text-primary">Privacy Policy
                                      </a>
                                    </div>

                                    <div class="col-md-12">
                                      <hr class="mt-3">
                                      <p class="text-black-50"> Made with ♥ by Think Plus
                                      </p>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </body>
                          </html>';
