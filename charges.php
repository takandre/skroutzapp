<?php
//charges.php
$api       = new Session(...);
$recurring = $api->recurring_application_charge;
$shop      = $api->shop->get();

//If they have accepted there should be a charge_id
if (isset($_GET['charge_id'])){
      $charge_id = $_GET['charge_id'];
      $theCharge = $recurring->get($charge_id);

      if ($theCharge['status'] == 'accepted'){
         $recurring->activate($charge_id);
      }else{
         //charge was not accepted
         //additionally you can add other functionality here for pending charges
         //and declined charges
      }
}else{
      $fields = array(
          'name' => 'Recurring Application Charge for My App',
          'price' => '1.00',
          'return_url' => 'http://www.mydomain.com/charges.php'
      );

      //If the shop is a test shop make the charge a test charge
      if ($shop['plan-name'] == 'development') $fields['test'] = 'true';
      $theCharge = $recurring->create($fields);
      if (isset($theCharge['confirmation-url'])){
           header("Location: " . $theCharge['confirmation-url']);
      }
}
?>
