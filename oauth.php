<?php
    session_start();
    require __DIR__ . '/vendor/autoload.php';
    use phpish\shopify;
    require __DIR__ . '/conf.php';


    //Start Create Variables Global
    $notInstallApp = true;
    $allPassJson = file_get_contents('allpass.json');
    $allPassArray = json_decode($allPassJson, true);
    $nameShop = $_GET['shop'];
    $titleShop = str_replace(".myshopify.com", "", $nameShop);
    $xmlURL = ' https://dev.think-plus.gr/skroutz_app_xml/xmlallsite/products-' . $titleShop . '.xml';

    //Search on allpass if shop have Installed and take the oauth
    for ($i = 0;$i < count($allPassArray);$i++)
    {
      if ( $allPassArray[$i]['shop'] == $_GET['shop'] )
      {
        $currentavailability = $allPassArray[$i]['availability'];
        $currentauthtoken = $allPassArray[$i]['authtoken'];
        $notInstallApp = false;
      }
    }

    //End Create Variables Global

    include 'check_app_installed.php';

    if ( $notInstallApp ){
      # Guard: http://docs.shopify.com/api/authentication/oauth#verification
      shopify\is_valid_request($_GET, SHOPIFY_APP_SHARED_SECRET) or die('Invalid Request! Request or redirect did not come from Shopify');

      # Step 2: http://docs.shopify.com/api/authentication/oauth#asking-for-permission
      if (!isset($_GET['code']))
      {
        //Create a history for all  installations of apps
        $permission_url = shopify\authorization_url($_GET['shop'], SHOPIFY_APP_API_KEY, array(
              'read_products'
        ));
        //GoBack in Admin Panel
        $permission_url = $permission_url . '&redirect_uri=' . SHOPIFY_REDIRECT_URL;
        die("<script> window.location.href='$permission_url'</script>");
      }

      # Step 3: http://docs.shopify.com/api/authentication/oauth#confirming-installation
      try
      {
        # shopify\access_token can throw an exception
        $oauth_token = shopify\access_token($_GET['shop'], SHOPIFY_APP_API_KEY, SHOPIFY_APP_SHARED_SECRET, $_GET['code']);
        $_SESSION['oauth_token'] = $oauth_token;
        $_SESSION['shop'] = $_GET['shop'];

        //Create a history for all  installations of apps
        // Append a new person to the file
        $current = array(
            'shop' => $_GET['shop'],
            'authtoken' => $_SESSION['oauth_token'],
            'apikey' => SHOPIFY_APP_API_KEY,
            'availability' => 'Delivery time 1-3 days'
        );

        // Write the contents back to the file
        $inp = file_get_contents('allpass.json');
        $tempArray = json_decode($inp, true);
        $jsonData = json_encode($tempArray);
        file_put_contents('allpass.json', $jsonData);
        array_push($tempArray, $current);
        $jsonData = json_encode($tempArray);
        file_put_contents('allpass.json', $jsonData);

        $urlexit = 'https://'.$_GET['shop'].'/admin/apps/export-product-xml';
        die("<script> window.location.href='$urlexit'</script>");
      }
      catch(shopify\ApiException $e)
      {
        # HTTP status code was >= 400 or response contained the key 'errors'
        echo $e;
        print_R($e->getRequest());
        print_R($e->getResponse());
      }
      catch(shopify\CurlException $e)
      {
        # cURL error
        echo $e;
        print_R($e->getRequest());
        print_R($e->getResponse());
      }
    } else {
      //Get All Product and crate file.XML
      include 'get_products.php';
      //App admin panel
      include 'appAdminPanel.php';
    }

?>
